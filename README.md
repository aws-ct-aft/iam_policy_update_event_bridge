## AFT Module for IAM Policy Update Overview

This module is to depoly necessary service in designated build account in AFT, to auto update IAM policy with IAM role ARN that created target account. For example, role A is created in account 11111111, that will be assumbed by 
role BuildAssumeRole in build acccount 22222222; the module is to update ResourceArn of BuildAssumeRole's IAM policy, so that BuildAssumeRole can only assume speficic role.  

## Implementation Detail 

1. When AFT customization create IAM role in the specific account, that require update ResourceArn in source Assuming IAM Policy of build account, it will create a System Manager Paramater in build account. The System Manager Parameter will conatins the target role ARN, and also the arn of assuming IAM policy
2. The system Manager will trigger event when parameter is created, updated or deleted.
3. The event bridge will capture this, and trigger Lambda function
4. The lambda will query the System Manager, pull out parameter value and update the IAM policy in assuming role; if the event type is delete, the lambda will remove the specific resource ARN

## How to deploy and clean up

1. Clone the entire folder into your local PC
2. Get the administrator CLI credential for build account
2. Run CLI commmand below

```
terraform init
terraform apply
```

3. To clean up

```
terraform destroy
```

## Contributor 

Jun Chen (jcchenn@amazon.com)

