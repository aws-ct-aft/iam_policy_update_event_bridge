#################################################################################################
#    Lambda function to get value from SSM, and update the resource Arn in build role policy,   #
#    the build role Arn is stored in SSM parameter name "BuildAssumePolicyArn".                 #
#################################################################################################

import json,boto3

def lambda_handler(event, context):
    
   iam = boto3.client('iam')
   ssm = boto3.client('ssm')

   # catpture parameter operation, and parameter name from Eventbridge
   parameter_operation = json.dumps(event['detail']['operation']).strip('\"')
   parameter_name = json.dumps(event['detail']['name']).strip('\"')

   # obtain the IAM policy arn, the assume policy name in SSM should not be hard coded,  
   BuildAssumePolicyArn = ssm.get_parameter(
               Name="BuildAssumePolicyArn", 
               WithDecryption=False
         )['Parameter']['Value']

   role_policy = iam.get_policy(
         PolicyArn=BuildAssumePolicyArn
      )
      
   policy_version = iam.get_policy_version(
         PolicyArn=BuildAssumePolicyArn, 
         VersionId=role_policy['Policy']['DefaultVersionId']
      )

   policy_doc = policy_version['PolicyVersion']['Document']

   # Updating policy
   if parameter_operation == "Create" or parameter_operation == "Update":
      
      # add resource arn into IAM policy
      ResourceArn = ssm.get_parameter(
         Name=parameter_name, 
         WithDecryption=False
      )['Parameter']['Value']
         
      print("ResourceArn for assume role is " + ResourceArn)
      print(policy_doc['Statement'][0]['Resource'])

      if ResourceArn in policy_doc['Statement'][0]['Resource']:
            print("Resource " + ResourceArn + " is exist in policy")
      else:
         print("add iam policy now for " + ResourceArn)
           
         # delete all old version of policy
         response = iam.list_policy_versions(
                  PolicyArn=BuildAssumePolicyArn
               )
         for policy_version in response['Versions']:
               if policy_version['IsDefaultVersion']:
                  continue
               response = iam.delete_policy_version(
                  PolicyArn=BuildAssumePolicyArn,
                  VersionId=policy_version['VersionId']
         )
         
         policy_doc['Statement'][0]['Resource'].insert(0,ResourceArn)
         
         response = iam.create_policy_version(
                  PolicyArn=BuildAssumePolicyArn,
                  PolicyDocument=json.dumps(policy_doc),
                  SetAsDefault=True
               )
   else:
      # remove the resource arn from IAM policy
      parameter_name_list = parameter_name.split('/')
      ResourceArn = "arn:aws:iam::"+parameter_name_list[2]+":role/"+parameter_name_list[3]

      if ResourceArn in policy_doc['Statement'][0]['Resource']:
         print("deleting iam policy now for " + ResourceArn)
         
         response = iam.list_policy_versions(
                  PolicyArn=BuildAssumePolicyArn
               )
         for policy_version in response['Versions']:
               if policy_version['IsDefaultVersion']:
                  continue
               response = iam.delete_policy_version(
                  PolicyArn=BuildAssumePolicyArn,
                  VersionId=policy_version['VersionId']
         )
         
         policy_doc['Statement'][0]['Resource'].remove(ResourceArn)
         
         response = iam.create_policy_version(
                  PolicyArn=BuildAssumePolicyArn,
                  PolicyDocument=json.dumps(policy_doc),
                  SetAsDefault=True
               )
      else:
         print("Resource " + BuildAssumePolicyArn + " is not exist in policy")
        

