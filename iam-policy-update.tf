data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

# create iam role for lambda
resource "aws_iam_role" "lambda_iam_role" {
    name = "lambda_iam_role"
    assume_role_policy = templatefile("${path.module}/lambda-iam-role-trust-policy.tpl", { none = "none" })
}

resource "aws_iam_role_policy" "lambda_iam_role_policy" {
    name = "lambda_iam_role_policy"
    role = aws_iam_role.lambda_iam_role.id
    policy = templatefile("${path.module}/lambda-iam-role-policy.tpl", { 
        current_region          = data.aws_region.current.name,
        current_account_id      = data.aws_caller_identity.current.account_id,
        current_function_name   = var.function_name 
    })
}

# define lambda function
data "archive_file" "lambda_iam_policy_update_code" {
    type        = "zip"
    source_dir  = "${path.module}/lambda"
    output_path = "${path.module}/archive/lambda-iam-policy-update-code.zip"
}

resource "aws_lambda_function" "lambda_iam_policy_update_func" {
    filename            = "${path.module}/archive/lambda-iam-policy-update-code.zip"
    source_code_hash    = data.archive_file.lambda_iam_policy_update_code.output_base64sha256
    function_name       = var.function_name
    role                = aws_iam_role.lambda_iam_role.arn
    handler             = "${var.function_name}.lambda_handler"
    runtime             = "python3.8"
    timeout             = 600
}

# define event bridge
resource "aws_cloudwatch_event_rule" "iam_policy_update_event_rule" {
    name        = "iam-policy-update-event-rule"
    description = "Capture event for paramter change"

    event_pattern = templatefile("${path.module}/event-pattern.tpl", { none = "none" })
}

resource "aws_cloudwatch_event_target" "iam_policy_update_event_target" {
    arn  = aws_lambda_function.lambda_iam_policy_update_func.arn
    rule = aws_cloudwatch_event_rule.iam_policy_update_event_rule.id
}

# add trigger
resource "aws_lambda_permission" "trigger_event_bridge" {
    statement_id  = "AllowTriggerFromEventBridge"
    action        = "lambda:InvokeFunction"
    function_name = aws_lambda_function.lambda_iam_policy_update_func.function_name
    principal     = "events.amazonaws.com"
    source_arn    = aws_cloudwatch_event_rule.iam_policy_update_event_rule.arn
}