{
  "detail": {
    "name": [{
      "prefix": "/BuildAccountUpdateResourceArn/"
    }],
    "operation": ["Create", "Update", "Delete"]
  },
  "detail-type": ["Parameter Store Change"],
  "source": ["aws.ssm"]
}